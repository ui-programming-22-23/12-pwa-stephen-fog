console.log("Shouts in Javascript");

var finnNoLeft = 0;
var finnNoRight = 0;
var finnNoFace = 0;
var points = 0;
var pointHold = -1;

let wackAudio = document.querySelector('#hitAudio');
let oopsAudio = document.querySelector('#oopsAudio');

var player = "Player";
var health = 5;
var finnTop = false;

var query = document.location.search;
console.log(query);

player = query.split("=")[1];
player = player.split("&")[0];
health = query.split("=")[2];
health = health.split("&")[0];
if(query.split("=")[3] == "on")
{
    finnTop=true;
    document.body.style.backgroundColor = "rgb(52,52,52)";
    document.getElementById("asideId").style.backgroundColor = "rgb(30,30,30)";
    document.getElementById("textId").style.color = "rgb(200,200,200)";
    document.getElementById("pointsText").style.color = "rgb(200,200,200)";
    document.getElementById("healthText").style.color = "rgb(200,200,200)";
    //document.getElementById("highText").style.color = "rgb(200,200,200)";
    document.getElementById("backIm").style.backgroundColor = "rgba(52, 52, 52, 0.9)";
}
else
{
    document.getElementById("backIm").style.backgroundColor = "rgba(207, 140, 185, 0.9)";
}

var holdString = player + " * " + health;
document.getElementById("healthText").innerHTML = holdString;

let highscore = 0;
if (typeof(Storage) !== "undefined") {
    if(player !== "")
    {
        if(localStorage.getItem(player) < 1)
        {
            localStorage.setItem(player, "0");
        }
    }
    else{
        localStorage.setItem("Player", "0");
        player = "Player";
    }
}
else{
    localStorage.setItem("Player", "0");
    player = "Player";
}



highscore = localStorage.getItem(player.toString());
//document.getElementById("scoreText").innerHTML = "highscore = " + highscore;


let setIntervalOnce = false;


function changeFinn() {
    if(!setIntervalOnce)
    {
        setIntervalOnce = true;
        setInterval(timerThing, 10);
    }
    var windowSizeX = window.innerWidth;
    var windowSizeY = window.innerHeight;

    var e = window.event;
    var posX = e.clientX;
    var posY = e.clientY;

    var newWidthOfFinn = windowSizeX * 0.5;

    var finnScaler = newWidthOfFinn / 600;

    var photoOriginX = windowSizeX * 0.4;

    var photoOriginY = windowSizeY - newWidthOfFinn;

    var taest = Math.sqrt((photoOriginY + 80 * finnScaler - posY) * (photoOriginY + 80 * finnScaler - posY) + (photoOriginX + 230 * finnScaler - posX) * (photoOriginX + 230 * finnScaler - posX))

    if (finnNoLeft == 2) {
        //230,80
        if (50 * finnScaler > Math.sqrt((photoOriginY + 80 * finnScaler - posY) * (photoOriginY + 80 * finnScaler - posY) + (photoOriginX + 230 * finnScaler - posX) * (photoOriginX + 230 * finnScaler - posX))) {
            points++;
            finnNoLeft = 0;
            wackAudio.play();
            document.getElementById("patFinnLeft").src = "imgs/finnArmLeft" + finnNoLeft + ".png";
            document.getElementById("pointsText").innerHTML = "Score = " + points;
        }
    }
    if (finnNoLeft == 4) {
        //130,200
        if (50 * finnScaler > Math.sqrt((photoOriginY + 200 * finnScaler - posY) * (photoOriginY + 200 * finnScaler - posY) + (photoOriginX + 130 * finnScaler - posX) * (photoOriginX + 130 * finnScaler - posX))) {
            points++;
            finnNoLeft = 0;
            wackAudio.play();
            document.getElementById("patFinnLeft").src = "imgs/finnArmLeft" + finnNoLeft + ".png";
            document.getElementById("pointsText").innerHTML = "Score = " + points;
        }

    }
    if (finnNoLeft == 5) {
        //160,490
        if (50 * finnScaler > Math.sqrt((photoOriginY + 490 * finnScaler - posY) * (photoOriginY + 490 * finnScaler - posY) + (photoOriginX + 160 * finnScaler - posX) * (photoOriginX + 160 * finnScaler - posX))) {
            points++;
            finnNoLeft = 0;
            wackAudio.play();
            document.getElementById("patFinnLeft").src = "imgs/finnArmLeft" + finnNoLeft + ".png";
            document.getElementById("pointsText").innerHTML = "Score = " + points;
        }

    }
    if (finnNoRight == 2) {
        //370,80
        if (50 * finnScaler > Math.sqrt((photoOriginY + 80 * finnScaler - posY) * (photoOriginY + 80 * finnScaler - posY) + (photoOriginX + 370 * finnScaler - posX) * (photoOriginX + 370 * finnScaler - posX))) {
            points++;
            finnNoRight = 0;
            wackAudio.play();
            document.getElementById("patFinnRight").src = "imgs/finnArmLeft" + finnNoRight + ".png";
            document.getElementById("pointsText").innerHTML = "Score = " + points;
        }

    }
    if (finnNoRight == 4) {
        //480,200
        if (50 * finnScaler > Math.sqrt((photoOriginY + 200 * finnScaler - posY) * (photoOriginY + 200 * finnScaler - posY) + (photoOriginX + 480 * finnScaler - posX) * (photoOriginX + 480 * finnScaler - posX))) {
            points++;
            finnNoRight = 0;
            wackAudio.play();
            document.getElementById("patFinnRight").src = "imgs/finnArmLeft" + finnNoRight + ".png";
            document.getElementById("pointsText").innerHTML = "Score = " + points;
        }

    }
    if (finnNoRight == 5) {
        //440,490
        if (50 * finnScaler > Math.sqrt((photoOriginY + 490 * finnScaler - posY) * (photoOriginY + 490 * finnScaler - posY) + (photoOriginX + 440 * finnScaler - posX) * (photoOriginX + 440 * finnScaler - posX))) {
            points++;
            finnNoRight = 0;
            wackAudio.play();
            document.getElementById("patFinnRight").src = "imgs/finnArmLeft" + finnNoRight + ".png";
            document.getElementById("pointsText").innerHTML = "Score = " + points;
        }

    }

    if (finnNoLeft == 0 && finnNoRight == 0 ||
        finnNoLeft == 0 && finnNoRight == 1 ||
        finnNoLeft == 0 && finnNoRight == 3 ||
        finnNoLeft == 1 && finnNoRight == 0 ||
        finnNoLeft == 1 && finnNoRight == 1 ||
        finnNoLeft == 1 && finnNoRight == 3 ||
        finnNoLeft == 3 && finnNoRight == 0 ||
        finnNoLeft == 3 && finnNoRight == 1 ||
        finnNoLeft == 3 && finnNoRight == 3) {
        while (finnNoLeft == 0 && finnNoRight == 0 ||
            finnNoLeft == 0 && finnNoRight == 1 ||
            finnNoLeft == 0 && finnNoRight == 3 ||
            finnNoLeft == 1 && finnNoRight == 0 ||
            finnNoLeft == 1 && finnNoRight == 1 ||
            finnNoLeft == 1 && finnNoRight == 3 ||
            finnNoLeft == 3 && finnNoRight == 0 ||
            finnNoLeft == 3 && finnNoRight == 1 ||
            finnNoLeft == 3 && finnNoRight == 3) {
            finnNoLeft = Math.floor((Math.random() * 6) + 0);
            finnNoRight = Math.floor((Math.random() * 6) + 0);
            finnNoFace = Math.floor((Math.random() * 4) + 0);
        }
        document.getElementById("patFinnLeft").src = "imgs/finnArmLeft" + finnNoLeft + ".png";

        document.getElementById("patFinnRight").src = "imgs/finnArmLeft" + finnNoRight + ".png";

        document.getElementById("patFinnFace").src = "imgs/finn-face-" + finnNoFace + ".png";

        document.getElementById("finnButton").innerHTML = " ";

        
        levelTime *= 0.99;
        timeLeft = levelTime;
        barElementWidth.style.width = "68%";
        console.log(levelTime);
        timeHurt = false
    }

    if (points > pointHold) {
        pointHold = points;
        if(pointHold > highscore){
            highscore = pointHold;
            document.getElementById("pointsText").innerHTML = "highscore = " + highscore;

            localStorage.setItem(player, highscore);
        }
    }
    else {
        oopsAudio.play();
        finnNoFace = 4;
        document.getElementById("patFinnFace").src = "imgs/finn-face-" + finnNoFace + ".png";
        health--;
        holdString = player + " * " + health;
        document.getElementById("healthText").innerHTML = holdString;
        if (health <= 0) {
        //     alert("Sorry, you DIED!");
        //     window.history.back();
            endWindow();
        }
    }
    
}


document.addEventListener('mouseup', function(event) {
    playing = true;
});

let song = new Audio;
song.src = "ASSETS/SOUNDS/bop.wav";
song.volume = 0.3;
let playing = false;
setInterval(playA, 1000);
function playA()
{
    if(playing)
    {
        song.play();
    }
}

let timeHurt = false;

let levelTime = 500;
let timeLeft = levelTime;
let barElementWidth = document.getElementById("timeBar");

function timerThing()
{
    timeLeft--;
    if(timeLeft < 1 && !timeHurt)
    {
        timeHurt = true;
        oopsAudio.play();
        finnNoFace = 4;
        document.getElementById("patFinnFace").src = "img/finn-face-" + finnNoFace + ".png";
        health--;
        holdString = player + " * " + health;
        document.getElementById("healthText").innerHTML = holdString;
        barElementWidth.style.width = "0%";
        if (health <= 0) {
        //     alert("Sorry, you DIED!");
        //     window.history.back();
            endWindow();
        }
    }
    else
    {
        barElementWidth.style.width = 68 * (timeLeft / levelTime) + "%";
    }
}


const resetBtn = document.querySelector('.resetHighscoreClass');
resetBtn.classList.add("play1");
resetBtn.addEventListener('click', () => {
    highscore = 0;
    localStorage.setItem(player,"0");
    points = 0;
    pointHold = 0;
    document.getElementById("pointsText").innerHTML = "highscore = " + highscore;
    resetBtn.style.display = "none";
    document.getElementById("highAlertId").innerHTML = highscore;
});

const againBtn = document.querySelector('.playAgainClass');
againBtn.classList.add("play1");
againBtn.addEventListener('click', () => {
    resetBtn.style.display = "inline";
    document.getElementById("GameOverBoxId").style.display = "none";
    document.getElementById("finnButton").style.display = "block";

    query = document.location.search;
    console.log(query);

    player = query.split("=")[1];
    player = player.split("&")[0];
    health = query.split("=")[2];
    health = health.split("&")[0];
    holdString = player + " * " + health;
    document.getElementById("healthText").innerHTML = holdString;
    document.getElementById("pointsText").innerHTML = "Score = 0";
    points = 0;
    pointHold = 0;
    
    levelTime = 500;
    timeLeft = levelTime;
});

function endWindow(){
    document.getElementById("GameOverBoxId").style.display = "block";
    document.getElementById("finnButton").style.display = "none";
    document.getElementById("pointAlertId").innerHTML = points;
    document.getElementById("highAlertId").innerHTML = highscore;
}